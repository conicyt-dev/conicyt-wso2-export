process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var _ = require('lodash');
var request = require('request');
var fs = require('fs');
var fstream = require('fstream');
var q = require('q');
var unzip = require('unzip');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

module.exports = function (apiName, apiVersion, apiProvider, amSocket, amUsername, amPassword) {
    var api = {name: apiName, version: apiVersion, provider: apiProvider};
    var defer = q.defer();

    var b = new Buffer(amUsername + ":" + amPassword);
    var auth = 'Basic ' + b.toString('base64');

    request(
        {
            uri: amSocket + '/api-import-export-v0.9.1/export-api',
            method: 'GET',
            headers: {
                "Authorization": auth
            },
            qs: api
        }, function (req, resp) {
            if (resp.statusCode == 200) {
                defer.resolve();
            }
            else {
                defer.reject(resp.statusCode);
            }
        })
        .pipe(unzip.Parse())
        .pipe(fstream.Writer(__dirname));

    return defer.promise;
};

